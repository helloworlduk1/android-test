﻿using Android.App;
using Android.Widget;
using Android.OS;
using Pilot.Droid.Fragments;

namespace Pilot.Droid
{
    [Activity(Label = "Pilot.Droid", MainLauncher = true)]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            var transaction = FragmentManager.BeginTransaction();
            transaction.Add(Resource.Id.frameContainer, new LoginFragment(), "LoginFragment");
            transaction.Commit();

        }


        
    }
}

