﻿using System.Collections.Generic;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Pilot.Droid.Model;

namespace Pilot.Droid.Fragments
{
    public class PeopleFragment : Fragment
    {

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            var view = inflater.Inflate(Resource.Layout.PeoplesCustomView, container, false);
            var peopleList = GetList();
            var listView = view.FindViewById<ListView>(Resource.Id.standardListView);
            listView.Adapter = new TestAdapter(this, peopleList);

            return view;

        }

        public List<People> GetList()
        {
            return new List<People>
            {
                new People
                {
                    Id = 1,
                    Age = 10,
                    Bio = "I am a young person at the age of 10 thinking to do something amazing",
                    Location = "Luton",
                    Name = "Jamie"
                },
                new People
                {
                    Id = 2,
                    Age = 15,
                    Bio = "Man like me is 15 and still going hard, you get me!",
                    Location = "Luton",
                    Name = "Dan"
                },
                new People
                {
                    Id = 3,
                    Age = 20,
                    Bio = "I am a student working full time",
                    Location = "Bedford",
                    Name = "Yasmin"
                },
                new People
                {
                    Id = 4,
                    Age = 30,
                    Bio = "Im full time worker with commitments with family, two kids and still going strong",
                    Location = "Stevenage",
                    Name = "Berty"
                },
                new People
                {
                    Id = 5,
                    Age = 40,
                    Bio = "Just doing me, with a company above my head",
                    Location = "London",
                    Name = "Coleman"
                },
                new People
                {
                    Id = 5,
                    Age = 14,
                    Bio = "Young, Living and what can i say",
                    Location = "Luton",
                    Name = "Jamie"
                },
                new People
                {
                    Id = 6,
                    Age = 19,
                    Bio = "I am a young person at the age of 10 thinking to do something amazine",
                    Location = "Luton",
                    Name = "Jamie"
                },
                new People
                {
                    Id = 7,
                    Age = 1,
                    Bio = "I am a young person at the age of 10 thinking to do something amazine",
                    Location = "Luton",
                    Name = "Jamie"
                },
                new People
                {
                    Id = 8,
                    Age = 100,
                    Bio = "I am a young person at the age of 10 thinking to do something amazine",
                    Location = "Luton",
                    Name = "Jamie"
                },
                new People
                {
                    Id = 9,
                    Age = 31,
                    Bio = "I am a young person at the age of 10 thinking to do something amazine",
                    Location = "Luton",
                    Name = "Jamie"
                },
                new People
                {
                    Id = 10,
                    Age = 50,
                    Bio = "I am a young person at the age of 10 thinking to do something amazine",
                    Location = "Luton",
                    Name = "Jamie"
                },
                new People
                {
                    Id = 11,
                    Age = 70,
                    Bio = "I am a young person at the age of 10 thinking to do something amazine",
                    Location = "Luton",
                    Name = "Jamie"
                },
                new People
                {
                    Id = 12,
                    Age = 10,
                    Bio = "I am a young person at the age of 10 thinking to do something amazine",
                    Location = "Luton",
                    Name = "Jamie"
                },
            };
        }
    }


}