﻿using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace Pilot.Droid.Fragments
{
    public class RegisterFragment : Fragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            var view = inflater.Inflate(Resource.Layout.RegisterFragmentlayout, container, false);
            var dataBtn = view.FindViewById<Button>(Resource.Id.SubmitData);
            dataBtn.Click += ShowData;
            return view;

        }

        private void ShowData(object sender, System.EventArgs e)
        {
            ReplaceFragment(new PeopleFragment());
        }

        public void ReplaceFragment(Fragment fragment)
        {
            if (fragment.IsVisible)
            {
                return;
            }

            var transaction = FragmentManager.BeginTransaction();
            transaction.Replace(Resource.Id.frameContainer, fragment);
            transaction.AddToBackStack(null);
            transaction.Commit();
        }
    }
}