﻿using System;
using System.Collections;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Pilot.Droid.Model;
using Object = Java.Lang.Object;

namespace Pilot.Droid.Fragments
{
    public class TestAdapter: BaseAdapter
    {
        List<People> items;
        Activity context;
        public TestAdapter(Fragment context, List<People> peoples) : base()
        {
            this.context = context.Activity;
            items = peoples;

        }
        public override Object GetItem(int position)
        {
            return position;
        }

        public override long GetItemId(int position)
        {
            return items[position].Id;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = items[position];
            View view = convertView;
            if (view == null)
            {
                view = context.LayoutInflater.Inflate(Resource.Layout.PeoplesCustomView, null);
            }

            view.FindViewById<TextView>(Resource.Id.peopleName).Text = item.Name;
            view.FindViewById<TextView>(Resource.Id.peopleId).Text = item.Id.ToString();
            view.FindViewById<TextView>(Resource.Id.peopleAge).Text = item.Age.ToString();
            view.FindViewById<TextView>(Resource.Id.peopleDescription).Text = item.Bio;
            view.FindViewById<TextView>(Resource.Id.peopleLocation).Text = item.Location;

            return view;
        }

        public override int Count
        {
            get { return items.Count; }
        }
    }
}