﻿using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace Pilot.Droid.Fragments
{
    public class LoginFragment : Fragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.LoginFragmentLayout, container, false);
            var RegisterBtn = view.FindViewById<Button>(Resource.Id.RegisterBtn);
            RegisterBtn.Click += RegisterEvent;
            return view;
        }

        private void RegisterEvent(object sender, System.EventArgs e)
        {
            ReplaceFragment(new RegisterFragment());
        }

        public void ReplaceFragment(Fragment fragment)
        {
            if (fragment.IsVisible)
            {
                return;
            }

            var transaction = FragmentManager.BeginTransaction();
            transaction.Replace(Resource.Id.frameContainer, fragment);
            transaction.AddToBackStack(null);
            transaction.Commit();
        }


    }
}