﻿namespace Pilot.Droid.Model
{
    public class People
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string Bio { get; set; }
        public string Location { get; set; }
    }
}